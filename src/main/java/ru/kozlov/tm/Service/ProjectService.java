package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Repository.ProjectRepository;

import java.util.Map;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(
            ProjectRepository projectRepository
    ) {
        this.projectRepository = projectRepository;
    }

    public String removeAllProjects() {
        projectRepository.removeAll();

        return "[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]";
    }

    public String createProjectByName(String name) {
        Project project = projectRepository.createByName(name);

        return "[ПРОЕКТ " + project.getName() + " СОЗДАН]";
    }

    public Map<Integer, Project> getAllProjects() {
        return projectRepository.getAll();
    }

    public String removeProjectById(int id) {
        projectRepository.removeById(id);

        return "[ПРОЕКТ #" + id + " УДАЛЕН]";
    }
}
