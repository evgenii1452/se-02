# TASK MANAGER
### СТЕК ТЕХНОЛОГИЙ
```
    maven 4.0.0
    java 1.8
    junit 4.11
```
### РАЗРАБОТЧИК
```
    Kozlov Evgeny
    evegnii-kozlov@bk.ru
```
### СБОРКА ПРИЛОЖЕНИЯ
```
    nvm install
```
### ЗАПУСК ПРИЛОЖЕНИЯ
```
    java -jar /path/to/task-manager/target/task-manager-1.0.0.jar
```
